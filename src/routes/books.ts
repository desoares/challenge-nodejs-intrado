import { Router } from 'express';
import { BookController } from '../controllers/BookController';
import { ErrorController } from '../controllers/ErrorController';
const controller = new BookController();
const errorController = new ErrorController();
export const router = Router();

router.post('/books', controller.create);
router.get('/books', controller.list);
router.get('/books/:id', controller.read);
router.patch('/books', controller.update);
router.patch('/books/:id', controller.updateOne);
router.delete('/books', controller.deleteMany);
router.delete('/books/:id', controller.delete);