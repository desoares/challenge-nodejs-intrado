import { BookModel } from './BookModel';

export class LibraryModel {
  private _library: any[];

  constructor () {
    this._library = [];
  }

  get library() {
    return this._library;
  }

  addBook(book: BookModel): void {
    this._library.push(book);
  }

  removeBook(id: number): BookModel {
    const deletingBook = this._library[id];

    this._library[id] = null;

    return deletingBook;
  }

  getLength(): number {
    return this._library?.length || 0;
  }

}