import { LibraryModel } from './LibraryModel';

export class BookModel {
  private readonly _id: number;
  private _name: string;
  private _author: string;
  private _genre: string;
  private _summary: string;

  constructor (library: LibraryModel, name: string, author: string, genre: string, summary: string) {
    this._id = library.getLength();
    this._name = name;
    this._author = author;
    this._genre = genre;
    this._summary = summary;

    if (!name || !author || !genre) {
      throw new Error(`MISSING REQUIRED FIELD: (name, author or genre)`);
    }

    library.addBook({
      id: this._id,
      name: this.name, 
      author: this.author,
      genre: this.genre,
      summary: this.summary
    } as BookModel);
  }

  private buildRequiredError(field: string | null): Error {
    return new Error(`BOOK ${field} IS REQUIRED.`);
  }

  get id(): number {
    return this._id;
  }

  public get name(): string {
    return this._name;
  }

  public get author(): string {
    return this._author;
  }

  public get genre(): string {
    return this._genre;
  }

  public get summary(): string {
    return this._summary;
  }

  public set name(name: string) {
    if (!name) {
      throw this.buildRequiredError('name');
    }

    this._name = name;
  }

  public set author(author: string) {
    if (!author) {
      throw this.buildRequiredError('author');
    }

    this._author = author;
  }

  public set genre(genre: string) {
    if (!genre) {
      throw this.buildRequiredError('genre');
    }

    this._genre = genre;
  }

  public set summary(summary: string) {
    this._summary = summary;
  }
}