import { BookModel, LibraryModel } from '../models/index';


export class BookRepository {
  public library: LibraryModel;

  constructor () {
    this.library = new LibraryModel();
  }

  private _filterLibrary(query: any): BookModel[] {
    const queryKeys = Object.keys(query);

    if (!queryKeys.length) {
      return this.library.library.filter(book => book);
    }

    return this.library.library.filter((book: any) => queryKeys.every((key: any) => book?.[key] === query[key]));
  }

  create(data: any): BookModel {
    const { name, author, genre, summary } = data;

    return new BookModel(this.library, name, author, genre, summary);
  }

  insert(data: any[]): BookModel[] {
    const start = this.library.library.length;

    data.forEach(book => {
      const { name, author, genre, summary } = book;
      new BookModel(this.library, name, author, genre, summary);
    });

    return this.library.library.slice(start, this.library.library.length);
  }

  find(query: any = {}): BookModel[] {
    return this._filterLibrary(query);
  }

  findOne(query: any = {}): BookModel | null {
    const queryKeys: string[] = Object.keys(query);

    if (!queryKeys.length) {
      return this.library.library[0] || null;
    }

    return this.library.library.filter((book: any) => queryKeys.every(key => book[key] == query[key]))[0] || null;
  }

  findById(id: number): BookModel | null {
    return this.library.library[id] || null;
  }

  findOneAndUpdate(id: number, query: any): BookModel | null {
    const updatindBook: any = this.library.library[id];
    if (!updatindBook) {
      return null;
    }

    Object.keys(query).forEach(key => updatindBook[key] = query[key]);

    return updatindBook;
  }

  updateMany(query: any, update: any): ModelResponse {
    const updatingBooks = this._filterLibrary(query);

    if (updatingBooks.length) {
      updatingBooks.forEach((updatindBook: any) => Object.keys(update).forEach(key => updatindBook[key] = update[key]));
    }

    return { affectedRows: updatingBooks.length };
  }

  deleteOne(id: number): ModelResponse {
    const removed = this.library.removeBook(id);

    return { affectedRows: removed ? 1 : 0 };
  }

  deleteMany(query: any): ModelResponse {
    const deletingBooks = this._filterLibrary(query);

    deletingBooks.forEach(book => this.library.removeBook(book.id));

    return { affectedRows: deletingBooks.length };
  }
}

interface ModelResponse {
  affectedRows: number;
}