import express from 'express';
import { json, urlencoded } from 'body-parser';
import { router } from './routes/books';
import { config } from 'dotenv';
import { ErrorController } from './controllers/ErrorController'

config({ path: `.${process.env.NODE_ENV || 'dev'}.env` });

const errorController = new ErrorController();

export class App {
  public app: express.Application;
  
  public constructor () {
    this.app = express();
    this.appMiddlewares();
    this.router();
  }

  private appMiddlewares() {
    this.app.use(json({ limit: '55mb' }));
    this.app.use(urlencoded({ extended: true, limit: '55mb' }));
  }

  private router() {
    this.app.use('/', router);
    this.app.use(errorController.errorParser);
  }
  
}

export default new App().app;