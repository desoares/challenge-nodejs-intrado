import app from './server'
import { config } from 'dotenv';

const isProd = (process.env.NODE_ENV === 'prod')
const path =  `${isProd ? '' : `.${process.env.NODE_ENV || ''}`}.env`;

config({ path });

export const listner = app.listen(
  process.env.PORT, () => console.log(`Application up and running! PORT: ${process.env.PORT}`)
);

export default app;
