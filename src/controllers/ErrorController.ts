import { Request, Response } from 'express';

export class ErrorController {
  errorParser(error: Error, _: any, res: Response, next: Function) {
    const { message } = error;
    let status: number;

    switch (true) {
      case message.includes('REQUIRED') : status = 400; break;
      case message.includes('NOT FOUND') : status = 404; break;
      case message.includes('NO BOOK') : status = 404; break;
      default : status = 500;
    }

    return res.status(status).json({ message, status });
  }
}
