import { Request, Response } from 'express';
import { BookRepository } from '../repositories/BookRepository';

const model = new BookRepository();

export class BookController {
  public create(req: Request, res: Response, next: Function): void {
    const newBook = model.insert([].concat(req.body));

    try {
      res.json(newBook);
    } catch(e) {
      next(e);
    }
  }

  public  list(req: Request, res: Response, next: Function): void {
    const bookList = model.find(req.query);

    res.json(bookList);
  }

  public  read(req: Request, res: Response, next: Function): void {
    const book = model.findById(parseInt(req.params.id, 10));

    if (!book) {
      return next(new Error(`NO BOOK WITH ID: ${req.params.id}`));
    }

    res.json(book);
  }

  public  updateOne(req: Request, res: Response, next: Function): void {
    const updatetingBook = model.findOneAndUpdate(parseInt(req.params.id, 10), req.body);

    if (!updatetingBook) {
      return next(new Error(`NO BOOK WITH ID: ${req.params.id}`));
    }

    res.json(updatetingBook);
  }

  public  update(req: Request, res: Response, next: Function): void {
    const { query, update } = req.body;
    const updateResylt = model.updateMany(query, update);

    res.json(updateResylt);
  }

  public  delete(req: Request, res: Response, next: Function): void {
    try {
      const deletionResult = model.deleteOne(parseInt(req.params.id, 10));
  
      res.json(deletionResult);
    } catch(e) {
      next(e);
    }
  }

  public  deleteMany(req: Request, res: Response, next: Function): void {
    try {
      const deletionResult = model.deleteMany(req.body);
  
      res.json(deletionResult);
    } catch(e) {
      next(e);
    }
  }
}
