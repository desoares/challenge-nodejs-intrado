module.exports = {
  parser: '@typecript-eslint/parsser',
  env: {
    node: true,
    es2021: true
  },
  plugins: ['@typescrit-eslint'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint'
  ],
  rules: {
  }
}
