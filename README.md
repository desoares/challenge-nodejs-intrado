# Challenge Nodejs Intrado
## Starting
In order to get the best expirience with this API you should use Linux, MacOS or Windows WSL terminal.
You must have Node.js and NPM installed in you environment. Take a look here on how to install [https://nodejs.org/en/](https://nodejs.org/en/).
All the scripts was tested with Yarn:

```bash
$ npm i -g yarn
```
After installing Yarn, install API dependencies:

```bash
$ yarn
yarn install v1.22.10
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
Done in 2.63s.
```

To launch the application in a production environment you should create a .env, using the .env.dev file as example for
the environment variables you will need. E.g.:

```.env
PORT=3000
```
Providing no NODE_ENV value, the application will launch in "dev".

You can run your production version building and then loading with NODE_ENV=prod:

```bash
$ yarn build
yarn run v1.22.10
$ tsc -p .
Done in 1.53s.
$ NODE_ENV=prod yarn load
yarn run v1.22.10
$ node dist/src/app
Application up and running! PORT: 8888

```
Or execute both commands at once 

```bash
$ yarn build && yarn load
yarn run v1.22.10
$ tsc -p .
Done in 1.51s.
yarn run v1.22.10
$ node dist/src/app
Application up and running! PORT: 8811

```

When developing new features you can load in dev:
```
$ yarn dev
yarn run v1.22.10
$ ts-node-dev src/app.ts
[INFO] 22:02:55 ts-node-dev ver. 1.1.6 (using ts-node ver. 9.1.1, typescript ver. 4.2.3)
Application up and running! PORT: 8811

```

...or test:dev

```
yarn run v1.22.10
$ nodemon --exec jest
[nodemon] 2.0.7
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): src/**/* test/**/*
[nodemon] watching extensions: ts
[nodemon] starting `jest`
 PASS  test/unit/BookRepository.test.ts
 PASS  dist/test/integration/books.test.js
  ● Console

    console.log
      Application up and running! PORT: 8813

      at Server.<anonymous> (dist/src/app.js:12:75)

 PASS  dist/test/unit/BookRepository.test.js
 PASS  test/unit/LibraryModel.test.ts
 PASS  test/unit/BookModel.test.ts
 PASS  dist/test/unit/LibraryModel.test.js
 PASS  dist/test/unit/BookModel.test.js

Test Suites: 7 passed, 7 total
Tests:       48 passed, 48 total
Snapshots:   0 total
Time:        0.991 s, estimated 1 s
Ran all test suites.
[nodemon] clean exit - waiting for changes before restart
```

Both options has watchers and will reload on TypeScript files change, in both src/ and test/ folders.

## Using the book API

The application persists the data in memory, so whenever you restart it all data will be lost.
Once you started it you can create, list, update and delete the books. The data is a simple schema witn an id, not provided by the client,
a name, genre and summary. In order to create a new book entry simply send a post to the HTTP address e.g.:

POST http://localhost:8811/books
```json
Content-Type application/json

{
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}
```
### RESPONSE
```json
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 87
ETag: W/"57-xEi0RkQZjjbtgdpdlD/bG+OMbBU"
Date: Thu, 25 Mar 2021 00:35:22 GMT
Connection: close

[
  {
    "id": 0,
    "name": "My book",
    "author": "Writer",
    "genre": "test",
    "summary": "Some big story"
  }
]
```

All methods accept one or many book entries but has specificities in some cases:

Create books:

POST http://localhost:8811/books
```json
Content-Type: application/json

[{
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}, {
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}, {
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}]
```

#### Get one book:

GET http://localhost:8811/1

#### List or search for books:

GET http://localhost:8811/books?genra=test


#### Update one book (must pass it id on the URI)

PATCH http://localhost:8811/books/0
```json
Content-Type: application/json

{
  "name": "My new book name",
  "author": "New author"
}
```

#### Update many books (must pass it id on the URI)

PATCH http://localhost:8811/books
```json
Content-Type: application/json

{
  "query": { "Author": "J. R. RTolkien" },
  "update": { "Author": "J. R. R. Tolkien" }
}
```
#### Delete one book

DELETE http://localhost:8811/books/12

Response:
```json
{
  "affectedRows": 1
}
```

#### Delete many books

DELETE http://localhost:8811/books
```json
Content-Type: application/json
{
  "Author": "J. R. RTolkien"
}
```

Response
```json
{
  "affectedRows": 4
}
```

For more examples refer to the rest files inside ["rest/"](rest/) folder