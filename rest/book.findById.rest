@host=http://localhost:8811

GET {{host}}/books/1

############## RESPONSE #############
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 85
ETag: W/"55-lv7AjCpWufZXW9u07n3vUzkhbv4"
Date: Wed, 24 Mar 2021 15:50:39 GMT
Connection: close

{
  "id": 1,
  "name": "My book",
  "author": "Writer",
  "genre": "test",
  "summary": "Some big story"
}