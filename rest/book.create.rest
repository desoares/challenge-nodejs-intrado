@uri=http://localhost:8811/books
POST {{uri}}
Content-Type: application/json

{
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}

############# RESPONSE ##################
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 87
ETag: W/"57-xEi0RkQZjjbtgdpdlD/bG+OMbBU"
Date: Thu, 25 Mar 2021 00:35:22 GMT
Connection: close

[
  {
    "id": 0,
    "name": "My book",
    "author": "Writer",
    "genre": "test",
    "summary": "Some big story"
  }
]
#########################################
POST {{uri}}
Content-Type: application/json

[{
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}, {
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}, {
  "name": "My book",
  "author": "Writer",
  "summary": "Some big story",
  "genre": "test"
}]

############# RESPONSE ##################
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 259
ETag: W/"103-xj9fS2qucPYQYuA3Rfxv5HqbYjI"
Date: Wed, 24 Mar 2021 15:47:56 GMT
Connection: close

[
  {
    "id": 0,
    "name": "My book",
    "author": "Writer",
    "genre": "test",
    "summary": "Some big story"
  },
  {
    "id": 1,
    "name": "My book",
    "author": "Writer",
    "genre": "test",
    "summary": "Some big story"
  },
  {
    "id": 2,
    "name": "My book",
    "author": "Writer",
    "genre": "test",
    "summary": "Some big story"
  }
]
