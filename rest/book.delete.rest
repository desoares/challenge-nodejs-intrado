@uri=http://localhost:8811/books
DELETE {{uri}}/1
Content-Type: application/json

############ RESPONSE #########################
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 18
ETag: W/"12-JECp4Yef0u2kzRg2X2rS8BnZ2xs"
Date: Thu, 25 Mar 2021 01:23:07 GMT
Connection: close

{
  "affectedRows": 1
}

########################################################

DELETE {{uri}}
Content-Type: application/json

{
  "genre": "Undefined"
}

###################### RESPONSE ##########################
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 18
ETag: W/"12-CVDk6aU8NWrwj1AJ6psmZYas1Lo"
Date: Thu, 25 Mar 2021 01:23:48 GMT
Connection: close

{
  "affectedRows": 2
}