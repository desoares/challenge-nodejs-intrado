import { BookModel, LibraryModel } from '../../src/models';
import { BookRepository } from '../../src/repositories/BookRepository';

describe('Book repository testing', () => {
  let model: BookRepository;

  beforeEach(() => {
    model = new BookRepository();
    const inserted = model.insert([
      { name: 'name1', author: 'author', genre: 'Love', sumary: 'summary' },
      { name: 'name2', author: 'author', genre: 'Hate', sumary: 'summary' },
      { name: 'name3', author: 'author', genre: 'Romance', sumary: 'summary' },
      { name: 'name4', author: 'author', genre: 'War', sumary: 'summary' },
      { name: 'name5', author: 'author', genre: 'Love', sumary: 'summary' }
    ]);

    expect(inserted.length).toBe(5);
    expect(inserted[0].name).toBe('name1');
    expect(inserted[4].name).toBe('name5');
  });

  it('Should create one repository', () => {
    const newBookModel = new BookRepository();

  });

  it('Should create a new book', () => {
    const newBook = model.create({ name: 'name', author: 'author', genre: 'genre', sumary: 'summary' });
  });

  it('should find all', () => {
    const allBooks = model.find();

    expect(allBooks.length).toBe(5);
    expect(allBooks[2].name).toBe('name3');
    expect(allBooks[3].name).toBe('name4');
  });

  it('Should find one book', () => {
    const name2 = model.find({ name: 'name2', genre: 'Hate' });
    expect(name2.length).toBe(1);
    expect(name2[0].name).toBe('name2');
  });

  it('Should find two love books', () => {
    const name2 = model.find({ genre: 'Love' });
    expect(name2.length).toBe(2);
    expect(name2[0].name).toBe('name1');
    expect(name2[1].name).toBe('name5');
  });

  it('Should make sure insert return newly inserted data', () => {
    const inserted = model.insert([
      { name: 'some', author: 'author', genre: 'test', sumary: 'summary' },
      { name: 'every', author: 'author', genre: 'it', sumary: 'summary' }
    ]);

    expect(inserted.length).toBe(2);
    expect(inserted[0].name).toBe('some');
    expect(inserted[1].genre).toBe('it');
    expect(model.library.library.length).toBe(7);
  })

  it('Should find books by id', () => {
    const book1 = model.findById(1);
    const book2 = model.findById(3);
    const book3 = model.findById(6);

    expect(book1?.id).toBe(1);
    expect(book2?.id).toBe(3);
    expect(book3).toBe(null);
  });

  it('Should find one book by name', () => {
    const book = model.findOne({ name: 'name3' });
    expect(book?.name).toBe('name3');
  });

  it('should update a book by id', () => {
    expect(model.library.library[0].name).toBe('name1');

    const updating = model.findOneAndUpdate(0, { name: 'Updated Version' });

    expect(updating?.name).toBe('Updated Version');
    expect(model.library.library[0].name).toBe(updating?.name);
  });

  it('Should update all books', () => {
    expect(model.library.library[0].genre).toBe('Love');
    expect(model.library.library[3].genre).toBe('War');

    const updateResult = model.updateMany({ author: 'author' }, { genre: 'reading' });

    expect(updateResult.affectedRows).toBe(5);
    expect(model.library.library[0].genre).toBe('reading');
    expect(model.library.library[3].genre).toBe('reading');
  });

  it('Should delete one book', () => {
    const deletionResult = model.deleteOne(1);

    expect(deletionResult.affectedRows).toBe(1);
    expect(model.library.library[1]).toBe(null);
  });

  it('Should delete all Love genre books', () => {
    const deletionResult = model.deleteMany({ genre: 'Love' });

    expect(deletionResult.affectedRows).toBe(2);
    expect(model.library.library[0]).toBe(null);
    expect(model.library.library[4]).toBe(null);
  });

  it('Should return an smaller set of data', () => {
    const deletionResult = model.deleteMany({ genre: 'Love' });
    const list = model.find();

    expect(deletionResult.affectedRows).toBe(2);
    expect(list.length).toBe(3);
  });

  it('Should not break when filtering with deleted books', () => {
    const deletionResult = model.deleteMany({ genre: 'Love' });
    const list = model.find({ author: 'author' });

    expect(deletionResult.affectedRows).toBe(2);
    expect(list.length).toBe(3);
  });
});