import { BookModel } from '../../src/models/index';
import { LibraryModel } from '../../src/models/LibraryModel';

describe('BookModel testing', () => {
  const library = new LibraryModel();
  const descBook = new BookModel(
    library,
    'The Lord of the Rings - The Two Throwers',
    'J.R. R. Tolkien',
    'Documentary',
    'I\'m not gonna spoil this one neither'
  );

  it('Should have an instance o book 0 in library', () => {
    const book = new BookModel(
        new LibraryModel(),
        'The Lord of the Rings - Fellowship of the Ring',
        'J.R. R. Tolkien',
        'Fantasy',
        'I\'m not gonna spoil!'
      );

      expect(book.name).toBe('The Lord of the Rings - Fellowship of the Ring');
      expect(book.id).toBe(0);
      expect(book).toBeInstanceOf(BookModel);
  });

  it('Should update a book', () => {
    expect(descBook.name).toBe('The Lord of the Rings - The Two Throwers');
    descBook.name = 'The Lord of the Rings - The Two Towers';
    expect(descBook.name).toBe('The Lord of the Rings - The Two Towers');
    expect(descBook.genre).toBe('Documentary');
    descBook.genre = 'Fantasy';
    expect(descBook.genre).toBe('Fantasy');
  });

  it('Should fail to update a book with no value for name', () => {
    try {
      descBook.name = '';
    } catch(e) {
      expect(e.message).toBe('BOOK name IS REQUIRED.');
    }
  });
});
