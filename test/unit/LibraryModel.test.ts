import { LibraryModel } from '../../src/models/index';
import { BookModel } from '../../src/models/index';

describe('Tests Library Model', () => {
  const library = new LibraryModel();

  it('Should create one empty Libery', () => {
    const empty = new LibraryModel();

    expect(empty instanceof LibraryModel).toBe(true);
    expect(empty.getLength()).toBe(0);
  });

  it('Should fill a library', () => {
    const olga = new BookModel(
      library,
      'Olga',
      'Fernando Moarais',
      'Biograthy',
      'Tells the story of Olga'
    );

    const cemAnosDeSolidao = new BookModel(
      library,
      'Cem Anos de Solidão',
      'G. G. Marquez',
      'Biograthy',
      'Tells the story of Olga'
    );

    expect(library.getLength()).toBe(2);
  })
});